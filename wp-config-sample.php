<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'database_name_here' );

/** MySQL database username */
define( 'DB_USER', 'username_here' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password_here' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Z/j@N<Yrw3Bk^2TZ7y]a|,qUe+a{M4o2o*O q3g{}}nMvyLt6t/:`I R%p:XTi4=' );
define( 'SECURE_AUTH_KEY',  'T[,2)r?s)r|]{)bPTs#LF)=fdDBmb5,^|c$ww:o/dr^TmIgJ!n!|rWS<gHL^y`%R' );
define( 'LOGGED_IN_KEY',    'hd5t-4]61# ]c4CsJd/N5%JI,KAn/v2!GAzPVphOYyJN!_z(Q<)fr7k2ip%B@gr9' );
define( 'NONCE_KEY',        'Djafqq94{]ZB5B&nkb&@*U0)k)T;{i;lAgG#[IK2Xp2;CXCE{@UjpmQ4:=pEP~{E' );
define( 'AUTH_SALT',        'kELl=cplRBCvm}Y#M@hO6>X!s7|$;U.kK=X`C3N ~P%7f_gAc`kt7*A6I>@;N`LI' );
define( 'SECURE_AUTH_SALT', '`Kd!*z!G}n ,i4+(Pqg8z[RhUf*}vtPAi5T&^!5w:nzS>*cf:_`wqTxF:tBtq^|=' );
define( 'LOGGED_IN_SALT',   '*Q6}]GEu0]=.^)h`hx-P8yy6VAa -=8t:kLdr]QVHL+<sJhl3wMY>mO}T-zD@7CS' );
define( 'NONCE_SALT',       'IOhU_j6J_;}g:$.CN}@C_t2THdtu!O^/%>d-Rjk!]4>42+%f L#i;7IN%U<vJFmA' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
