<?php
/**
 * Displays the header content
 *
 * @package Theme Freesia
 * @subpackage Photograph
 * @since Photograph 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php
function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
$photograph_settings = photograph_get_theme_options(); ?>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php endif;
wp_head(); ?>
</head>
<style>
    .side-menu > #site-branding{
        display: none ;
    }
</style>
<body <?php body_class(); ?>>
	<?php
	if ( function_exists( 'wp_body_open' ) ) {

		wp_body_open();

	} ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#site-content-contain"><?php esc_html_e('Skip to content','photograph');?></a>
	<!-- Masthead ============================================= -->
	<header id="masthead" class="site-header clearfix" role="banner">
		<?php if ( get_header_image() ) : ?>
		<a href="<?php echo esc_url(home_url('/'));?>" rel="home"><img src="<?php header_image(); ?>" class="header-image" width="<?php echo esc_attr(get_custom_header()->width);?>" height="<?php echo esc_attr(get_custom_header()->height);?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display'));?>"> </a>
	<?php endif; ?>
		<div class="header-wrap">

			<!-- Top Header============================================= -->
			<div class="top-header">

				<!-- Main Header============================================= -->
				<div id="sticky-header" class="clearfix">
					<div class="main-header clearfix">
                        <style>
                            #site-branding{
                                /*margin-left: -1% !important;*/
                            }

                        </style>
                        <?php
                        if(isMobile())
                        {echo '
 <div id="site-branding" style="margin-left: -1% !important; padding-top: 3%">
                            <a href="http://olivia.webup-dev.pl/" class="custom-logo-link" rel="home">
                                <img width="2138" height="921" src="http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia.png" class="custom-logo" alt="Olivia Oliver Web Page" srcset="http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia.png 2138w, http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia-300x129.png 300w, http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia-1024x441.png 1024w, http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia-768x331.png 768w, http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia-1536x662.png 1536w, http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia-2048x882.png 2048w" sizes="(max-width: 2138px) 100vw, 2138px">
                            </a>
                            <div id="site-detail">
                                <h1 id="site-title">
                                    <a href="http://olivia.webup-dev.pl/" title="Olivia Oliver Web Page" rel="home"> Olivia Oliver Web Page </a>
                                </h1>  
                            </div></div>';
                        }
                        else{ echo '
 <div id="site-branding">
                            <a href="http://olivia.webup-dev.pl/" class="custom-logo-link" rel="home">
                                <img width="2138" height="921" src="http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia.png" class="custom-logo" alt="Olivia Oliver Web Page" srcset="http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia.png 2138w, http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia-300x129.png 300w, http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia-1024x441.png 1024w, http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia-768x331.png 768w, http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia-1536x662.png 1536w, http://olivia.webup-dev.pl/wp-content/uploads/2020/03/cropped-olivia-2048x882.png 2048w" sizes="(max-width: 2138px) 100vw, 2138px">
                            </a>
                            <div id="site-detail">
                                <h1 id="site-title">
                                    <a href="http://olivia.webup-dev.pl/" title="Olivia Oliver Web Page" rel="home"> Olivia Oliver Web Page </a>
                                </h1>  <!-- end .site-title -->
                            </div></div>';
                        }
                        ?>


							<!-- Main Nav ============================================= -->
							<?php
							if($photograph_settings['photograph_disable_main_menu']==0){ ?>
								<nav id="site-navigation" class="main-navigation clearfix" role="navigation" aria-label="<?php esc_attr_e('Main Menu','photograph');?>">

								<button type="button" style="display: none !important" class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
									<span class="line-bar" style="display: none !important"></span>
							  	</button>
							  	<!-- end .menu-toggle -->
								<?php if (has_nav_menu('primary')) {
									$args = array(
									'theme_location' => 'primary',
									'container'      => '',
									'items_wrap'     => '<ul id="primary-menu" class="menu nav-menu">%3$s</ul>',
									); ?>

									<?php wp_nav_menu($args);//extract the content from apperance-> nav menu
									} else {// extract the content from page menu only
									wp_page_menu(array('menu_class' => 'menu', 'items_wrap'     => '<ul id="primary-menu" class="menu nav-menu">%3$s</ul>'));
									} ?>
								</nav> <!-- end #site-navigation -->
							<?php }
							$photograph_side_menu = $photograph_settings['photograph_side_menu'];
							$search_form = $photograph_settings['photograph_search_custom_header'];
							if( (1 != $photograph_settings['photograph_disable_main_menu']) || (1 != $search_form) ){  ?>
								<div class="right-toggle">
									<?php if($photograph_settings['photograph_disable_main_menu']==0){ ?>
<!--									<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">-->
<!--										<span class="line-bar"></span>-->
<!--								  	</button>-->
								  	<!-- end .menu-toggle -->
								  	<?php }
								  	if (1 != $search_form) { ?>
									<button type="button" id="search-toggle" class="header-search"></button>
									<?php }
									if(1 != $photograph_side_menu){ ?>

                                        <?php

                                        if(isMobile()){
									    echo '
 <div class="social-links clearfix" style="margin-left: -30% !important; margin-top: 5% !important" >
    <ul style="margin-left: -90%">
     <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-130"><a href="https://www.facebook.com/OliviaRybickaOliver%20"><span class="screen-reader-text">facebook</span></a></li>
     <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-134"><a href="https://www.instagram.com/livoliverr_"><span class="screen-reader-text">instagram</span></a></li>
    </ul>	
 </div>
<button type="button" style="margin-left: 15% !important; margin-top: 6% !important"class="show-menu-toggle"><span class="sn-text"><?php _e(\'Menu Button\',\'photograph\'); ?></span>
										<span class="bars"></span>
								  	</button>';
									    }else{
                                            echo '';
									    }
                                        ?>

								  	<?php } ?>
								</div>
								<!-- end .right-toggle -->
								<?php
							} ?>
					</div> <!-- end .main-header -->
				</div> <!-- end #sticky-header -->
			</div> <!-- end .top-header -->
			<?php if (1 != $search_form) { ?>
				<div id="search-box" class="clearfix">
					<button type="button" class="search-x"></button>
						<?php get_search_form();?>
				</div>
			<?php }

			$photograph_side_menu = $photograph_settings['photograph_side_menu'];
			if(1 != $photograph_side_menu){ ?>
				<aside class="side-menu-wrap" role="complementary">
					<div class="side-menu">
				  		<button type="button" class="hide-menu-toggle">
				  		<span class="screen-reader-text"><?php esc_html_e('Close Side Menu','photograph');?></span>		<span class="bars"></span>
					  	</button>

						<?php do_action ('photograph_new_site_branding');

						if (has_nav_menu('side-nav-menu') || (has_nav_menu( 'social-link' ) && $photograph_settings['photograph_side_menu_social_icons'] == 0 ) || is_active_sidebar( 'photograph_side_menu' ) ):

							if (has_nav_menu('side-nav-menu')) {
								$args = array(
									'theme_location' => 'side-nav-menu',
									'container'      => '',
									'items_wrap'     => '<ul class="side-menu-list">%3$s</ul>',
									); ?>
							<nav class="side-nav-wrap" role="navigation" aria-label="<?php esc_html_e('Sidebar Menu','photograph');?>">
								<?php wp_nav_menu($args); ?>
							</nav><!-- end .side-nav-wrap -->
							<?php }
							if($photograph_settings['photograph_side_menu_social_icons'] == 0):
								do_action('photograph_social_links');
							endif;

							if( is_active_sidebar( 'photograph_side_menu' )) {
								echo '<div class="side-widget-tray">';
									dynamic_sidebar( 'photograph_side_menu' );
								echo '</div> <!-- end .side-widget-tray -->';
							}
						endif; ?>
					</div><!-- end .side-menu -->
				</aside><!-- end .side-menu-wrap -->
				<?php
			} ?>
		</div><!-- end .header-wrap -->
        <style>

        </style>
		<?php
		if($photograph_settings['photograph_top_social_icons'] == 0):
			echo '<div class="header-social-block">';
				do_action('photograph_social_links');
			echo '</div>'.'<!-- end .header-social-block -->';
		endif;

		$photograph_enable_slider = $photograph_settings['photograph_enable_slider'];
		if (($photograph_settings['photograph_slider_video_display'] == 'video') && ($photograph_enable_slider=='frontpage'|| $photograph_enable_slider=='enitresite') ){
			if(is_front_page() && ($photograph_enable_slider=='frontpage') ) { ?>
				<!-- Video and image header ============================================= -->
				<?php photograph_video_category_sliders();

			} elseif($photograph_enable_slider=='enitresite'){

				photograph_video_category_sliders();

			}
		} else { ?>
		<!-- Main Slider ============================================= -->
		<?php

			if ($photograph_enable_slider=='frontpage'|| $photograph_enable_slider=='enitresite'){
				 if(is_front_page() && ($photograph_enable_slider=='frontpage') ) {

				 	if(is_active_sidebar( 'slider_section' )){

				 		dynamic_sidebar( 'slider_section' );

				 	} else {

				 		if($photograph_settings['photograph_slider_type'] == 'default_slider') {
							photograph_category_sliders();

						} else {

							if(class_exists('Photograph_Plus_Features')):
								do_action('photograph_image_sliders');
							endif;
						}

				 	}

				}
				if($photograph_enable_slider=='enitresite'){

					if(is_active_sidebar( 'slider_section' )){

				 		dynamic_sidebar( 'slider_section' );

				 	} else {

				 		if($photograph_settings['photograph_slider_type'] == 'default_slider') {

								photograph_category_sliders();

						} else {

							if(class_exists('Photograph_Plus_Features')):

								do_action('photograph_image_sliders');

							endif;
						}
				 	}


				}
			} ?>

		<?php } ?>
		<button type="button" class="scroll-down" type="button"><span><?php esc_html_e('menu','photograph');?></span><span></span><span></span></button><!-- Scroll Down Button -->
	</header> <!-- end #masthead -->
	<!-- Main Page Start ============================================= -->
	<div id="site-content-contain" class="site-content-contain">
<div id="content" class="site-content">


