<?php
/**
 * Template Name: Main
 *
 * @package Theme Freesia
 * @subpackage Photograph
 * @since Photograph 1.0
 */

get_header(); ?>

<?php
function isMobilEe() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

?>
        <div id="primary" class="content-area">
            <div class="contener">
                <?php
                if(isMobilEe()){
                    echo ' <div  style="background-color: black; height: 20px">
 
</div>
<div class="imageee" style="float: left; width:100%;
   
    position: relative;">
    <img class="hejka" style=" display: block; width:100%" src="'.get_bloginfo('wpurl').'/wp-content/'.wp_basename( $uploads['baseurl'] ).'/2018/04/Photo-A-scaled.jpg"/>
     <div class="sport" style=" position: absolute;
    
    width: 98%;
    padding: 1%;
    text-align: center;
    top:95%;
    left: 50%;
    font-size: 20px;
    color: #fff;  
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    ">
            <p style="font-size: 56px; color:white !important"><b>Olivia Oliver</b></p>
           
      </div>
</div>



 <div style="margin-top: 5%;">
 
 <style>
 .test{
 text-align: center;
 font-size: 30px;
 color: black;
 }
</style>
<div class="mt-1" style="clear: both"></div>
 <div class="cojest" style="background-color: black; height: 20px; width:100%; float: left; position: absolute">
 
</div>
<div style="margin-top: 15%;">
 <p class="test" style="line-height: 90%">International Ice-Dancer</p>
 
 <p class="test" style="line-height: 90%">Model, Humanitarian</p>
 <p class="test" style="line-height: 90%">World Record Holder</p>
 <p class="test" style="line-height: 90%">National Team Member</p>
 <p class="test" STYLE=" font-family: \'MyWebFont\', Fallback, sans-serif;; font-size: 2a6px;"><b><i>Passion is everything</i></b></p>
 
 
 
 
</div>
';
                }
                else {
               echo ' <img class="full" src="'.get_bloginfo('wpurl').'/wp-content/'.wp_basename( $uploads['baseurl'] ).'/2018/04/photograph-bg.jpg"/>';
                    }
            ?>
            </div>
        </div> <!-- #primary -->


<style>
    .hejka{
        width:400px;

        padding: 0 !important;
    }
    .full{
        width: 100% !important;
    }
    #primary{
        margin-right: 0% !important;
        width: 100%;
    }
</style>
</div>
<?php
/**
 * The template for displaying the footer.
 *
 * @package Theme Freesia
 * @subpackage Photograph
 * @since Photograph 1.0
 */

$photograph_settings = photograph_get_theme_options(); ?>
</div><!-- end #content -->
<!-- Footer Start ============================================= -->
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="widget-wrap">
            <div class="wrap">
                <div class="widget-area">
                    <?php
                    if($footer_column == '1' || $footer_column == '2' ||  $footer_column == '3' || $footer_column == '4'){
                        echo '<div class="column-'.absint($footer_column).'">';
                        if ( is_active_sidebar( 'photograph_footer_1' ) ) :
                            dynamic_sidebar( 'photograph_footer_1' );
                        endif;
                        echo '</div><!-- end .column'.absint($footer_column). '  -->';
                    }
                    if($footer_column == '2' ||  $footer_column == '3' || $footer_column == '4'){
                        echo '<div class="column-'.absint($footer_column).'">';
                        if ( is_active_sidebar( 'photograph_footer_2' ) ) :
                            dynamic_sidebar( 'photograph_footer_2' );
                        endif;
                        echo '</div><!--end .column'.absint($footer_column).'  -->';
                    }
                    if($footer_column == '3' || $footer_column == '4'){
                        echo '<div class="column-'.absint($footer_column).'">';
                        if ( is_active_sidebar( 'photograph_footer_3' ) ) :
                            dynamic_sidebar( 'photograph_footer_3' );
                        endif;
                        echo '</div><!--end .column'.absint($footer_column).'  -->';
                    }
                    if($footer_column == '4'){
                        echo '<div class="column-'.absint($footer_column).'">';
                        if ( is_active_sidebar( 'photograph_footer_4' ) ) :
                            dynamic_sidebar( 'photograph_footer_4' );
                        endif;
                        echo '</div><!--end .column'.absint($footer_column).  '-->';
                    }
                    ?>
                </div> <!-- end .widget-area -->
            </div><!-- end .wrap -->
        </div> <!-- end .widget-wrap -->

    <div class="site-info"  <?php if($photograph_settings['photograph_img-upload-footer-image'] !=''){?>style="background-image:url('<?php echo esc_url($photograph_settings['photograph_img-upload-footer-image']); ?>');" <?php } ?>>
        <div class="wrap">
            <div class="copyright-wrap clearfix">
                <?php

                if ( is_active_sidebar( 'photograph_footer_options' ) ) :
                    dynamic_sidebar( 'photograph_footer_options' );
                else: ?>
                    <div style="text-align: center;">
                        <p style="text-align: center;">Olivia is proudly sponsored by:</p>
						<?php echo '
                        <img src="'.get_bloginfo('wpurl').'/wp-content/'.wp_basename( $uploads['baseurl'] ).'/2020/04/logoo.png" style="width:230px;" />'; ?>
                    </div>
                    <div class="copyright">
                        <a title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" target="_blank" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo get_bloginfo( 'name', 'display' ); ?></a> |
                        <?php esc_html_e('Designed by:','photograph'); ?> <a title="<?php echo esc_attr__( 'WebUp', 'photograph' ); ?>" target="_blank" href="<?php echo esc_url( 'mys.pl' ); ?>"><?php esc_html_e('Webup','photograph');?></a> |
                        <?php  date_i18n(__('Y','photograph')) ; ?> <a title="<?php echo esc_attr__( 'WordPress', 'photograph' );?>" target="_blank" href="<?php echo esc_url( 'https://wordpress.org' );?>"><?php esc_html_e('WordPress','photograph'); ?></a> | <?php echo '&copy; ' . esc_attr__('Copyright All right reserved ','photograph'); ?>
                        <?php
                        if ( function_exists( 'the_privacy_policy_link' ) ) {
                            the_privacy_policy_link( ' | ', '<span role="separator" aria-hidden="true"></span>' );
                        }
                        ?>
                    </div>
                <?php endif;

                if($photograph_settings['photograph_buttom_social_icons'] == 0):
                    do_action('photograph_social_links');
                endif; ?>
            </div> <!-- end .copyright-wrap -->
            <div style="clear:both;"></div>
        </div> <!-- end .wrap -->
    </div> <!-- end .site-info -->
    <?php
    $disable_scroll = $photograph_settings['photograph_scroll'];
    if($disable_scroll == 0):?>
        <button type="button" class="go-to-top">
            <span class="icon-bg"></span>
            <i class="fa fa-angle-up back-to-top-text"></i>
            <i class="fa fa-angle-double-up back-to-top-icon"></i>
        </button>
    <?php endif; ?>
    <div class="page-overlay"></div>
</footer> <!-- end #colophon -->
</div><!-- end .site-content-contain -->
</div><!-- end #page -->
<?php wp_footer(); ?>
</body>
</html>
