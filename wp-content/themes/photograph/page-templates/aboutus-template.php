<?php
/**
 * Template Name: About Us Template
 *
 * Displays the About Us page template.
 *
 * @package Theme Freesia
 * @subpackage Photograph
 * @since Photograph 1.0
 */
get_header();
?>
<style>
    .check{
        margin-left: -20%; margin-right: 16% !important; margin-top: -8% !important; color: black !important; text-align: left; float: left; background-image: url('http://olivia.webup-dev.pl/wp-content/uploads/2020/03/bg-1-scaled.jpg') !important; z-index: 999;
    }
</style>
<div class="header-social-block">	<div class="social-links clearfix">
	<ul><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-130"><a href="https://www.facebook.com/OliviaRybickaOliver%20"><span class="screen-reader-text">facebook</span></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-134"><a href="https://www.instagram.com/livoliverr_"><span class="screen-reader-text">instagram</span></a></li>
</ul>	</div><!-- end .social-links -->
	</div>
<?php
$attachment_id = get_post_thumbnail_id();
$image_attributes = wp_get_attachment_image_src($attachment_id,'full'); ?>
<div <?php post_class('about-contant'); if(has_post_thumbnail()){ ?> style="background-image:url('<?php echo esc_url($image_attributes[0]); ?>');" <?php } ?>>
    <?php

    if(isMobile())
    {
        echo '
        <div class="wrap" style="text-align: center; color: black !important; align-content: center !important; width: 100% !important">
			<div id="primary" style="text-align: center; color: black !important; align-content: center !important; width: 100% !important" class="content-area">
			<main id="main" style="text-align: center; color: black !important; align-content: center !important; width: 100% !important" class="site-main" role="main">
				<article>
					<header class="page-header">
                        <h1 class="page-title page-page-page ">Meet Olivia</h1>
					</header><!-- .page-header -->
					<div class="entry-content" style="margin-right: 0% !important">
                        <div style="text-align: center; color: black !important; align-content: center !important; width: 100% !important"><h1 style="color: black !important;"><b>Meet Olivia</b></h1><strong>Age: 17</br>
                            </strong><strong>Born: Warsaw, Poland</br>
                            </strong><strong>Hometown: Barrie, Ontario, Canada</br>
                            </strong><strong>Nationalities: Polish, Canadian</br>
                            </strong><strong>Languages: English, Polish, French</strong></br>
                            <img class="hejka" style=" display: block; padding-top: 16%" src="'.get_bloginfo('wpurl').'/wp-content/'.wp_basename( $uploads['baseurl'] ).'/2020/05/mediaquery.jpg"/>
                            <div class="cojest" style="background-color: black; height: 20px; width:100%; float: left; position: absolute">
                            </br>
                             <div style="color: black !important; text-align: left !important;">
                                <div style="color: black !important;">
                                    <div style="color: black !important;">
                                        <div style="color: black !important;">
                                            <div style="color: black !important;">

                                                <p></br>Olivia Oliver is an international ice-dancer, model, charity founder, community hero and world record holder
                                                <div style="color: black !important;">

                                                    <p>Olivia began skating at the age of 5 and by the age of 11 she was the Provincial and Atlantic Canada Juvenile Free Skate Champion. On January 19, 2015, a few weeks before her 12<sup>th</sup> birthday, Olivia broke the Guinness World Record for the fastest spin on ice at the National Stadium in Warsaw, Poland. Olivia’s record speed was timed&nbsp;at 342 RPM, breaking the previous world record set in 2006 by 34 RPM and, to this day, Olivia’s record remains unbeaten!
                                                    <div style="color: black !important;">

                                                        <p>Using the world record challenge as a platform, Olivia raised over $30,000 towards making the wishes come true for children suffering from terminal illnesses or life-limiting conditions in Poland. Olivia also granted the wish of a 9 year-old girl with spina bifida by donating her Guinness World Record figure skate and Guinness Certificate.
                                                        <div style="color: black !important;">

                                                            <p>Olivia was subsequently awarded the Scotiabank Young Community Hero Award for her achievements and Olivia received personal recognition in the House of Commons from the speaker of the House and parliamentary members.
                                                            <div style="color: black !important;">

                                                                <p>In 2017 Olivia made the switch to ice-dance and began training full time at the Mariposa International Training Center in Barrie, Ontario. In her first season in ice-dance Olivia partnered with Petr Paleev, who moved from Moscow, Russia, to skate with Olivia and they qualified for the World Junior Ice Dance Championships in Zagreb, Croatia.
                                                                <div style="color: black !important;">

                                                                    <p>Olivia was one of only 30 Canadian recipients of the Dairy Farmers of Canada “Champions Fund Grant” awarded to deserving female athletes for the 2017/18 season.
                                                                    <div style="color: black !important;">

                                                                        <p>Olivia was chosen as a City Youth Ambassador travelling to Japan in the summer of 2018.
                                                                        <div style="color: black !important;">

                                                                            <p>In May, 2019 Olivia formed a new ice-dance partnership with Joshua Andari. Olivia and Josh were chosen to represent Poland at two Junior Grand Prix international events and, within a short period of time, they won the silver medal at the Polish National Junior Championships and attained the qualifying scores for the Junior World Championships.
                                                                            <div style="color: black !important;">

                                                                                <p>After the Grand Prix event in Gdansk, Poland, Olivia launched the charitable cause “Skating for Young Heroes”. Working in conjunction with the Polish children’s charity Fundacja Dziecięca Fantazja, “Skating for Young Heroes” brings joy and love to children, aged 3 - 18, who have a terminal illness or life-limiting condition. Olivia and Josh visited the homes of 9 terminally-ill children in Poland to spend time with the children and to make their dreams come true by presenting them with their material wish gifts including: laptops, smartphones, gaming consoles, bedroom furniture, autographed sports and entertainment memorabilia, toys and games.
                                                                                <div style="color: black !important;">

                                                                                    <p>In December, 2019, through “Skating for Young Heroes”, Olivia and Josh were guests of honor at a Gala Christmas Party in Warsaw, Poland, for over 300 sick children and handed out presents to all the children. The event is the largest of its kind in Europe.
                                                                                    <div style="color: black !important;">Olivia combines her full time training and competition schedule with her modeling and humanitarian work whilst excelling with her high school education.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                               
                        
        ';

    }else
        {
            echo '
            <div class="wrap">
			<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<article>
					<header class="page-header">
                        <h1 class="page-title page-page-page ">Meet Olivia</h1>
					</header><!-- .page-header -->
					<div class="entry-content">
                        <div class="check"><h1 style="color: black !important;"><b>Meet Olivia</b></h1><strong>Age: 17</br>
                            </strong><strong>Born: Warsaw, Poland</br>
                            </strong><strong>Hometown: Barrie, Ontario, Canada</br>
                            </strong><strong>Nationalities: Polish, Canadian</br>
                            </strong><strong>Languages: English, Polish, French</strong></br>
                            <div style="color: black !important;">
                                <div style="color: black !important;">
                                    <div style="color: black !important;">
                                        <div style="color: black !important;">
                                            <div style="color: black !important;">

                                                <p></br>Olivia Oliver is an international ice-dancer, model, charity founder, community hero and world record holder
                                                <div style="color: black !important;">

                                                    <p>Olivia began skating at the age of 5 and by the age of 11 she was the Provincial and Atlantic Canada Juvenile Free Skate Champion. On January 19, 2015, a few weeks before her 12<sup>th</sup> birthday, Olivia broke the Guinness World Record for the fastest spin on ice at the National Stadium in Warsaw, Poland. Olivia’s record speed was timed&nbsp;at 342 RPM, breaking the previous world record set in 2006 by 34 RPM and, to this day, Olivia’s record remains unbeaten!
                                                    <div style="color: black !important;">

                                                        <p>Using the world record challenge as a platform, Olivia raised over $30,000 towards making the wishes come true for children suffering from terminal illnesses or life-limiting conditions in Poland. Olivia also granted the wish of a 9 year-old girl with spina bifida by donating her Guinness World Record figure skate and Guinness Certificate.
                                                        <div style="color: black !important;">

                                                            <p>Olivia was subsequently awarded the Scotiabank Young Community Hero Award for her achievements and Olivia received personal recognition in the House of Commons from the speaker of the House and parliamentary members.
                                                            <div style="color: black !important;">

                                                                <p>In 2017 Olivia made the switch to ice-dance and began training full time at the Mariposa International Training Center in Barrie, Ontario. In her first season in ice-dance Olivia partnered with Petr Paleev, who moved from Moscow, Russia, to skate with Olivia and they qualified for the World Junior Ice Dance Championships in Zagreb, Croatia.
                                                                <div style="color: black !important;">

                                                                    <p>Olivia was one of only 30 Canadian recipients of the Dairy Farmers of Canada “Champions Fund Grant” awarded to deserving female athletes for the 2017/18 season.
                                                                    <div style="color: black !important;">

                                                                        <p>Olivia was chosen as a City Youth Ambassador travelling to Japan in the summer of 2018.
                                                                        <div style="color: black !important;">

                                                                            <p>In May, 2019 Olivia formed a new ice-dance partnership with Joshua Andari. Olivia and Josh were chosen to represent Poland at two Junior Grand Prix international events and, within a short period of time, they won the silver medal at the Polish National Junior Championships and attained the qualifying scores for the Junior World Championships.
                                                                            <div style="color: black !important;">

                                                                                <p>After the Grand Prix event in Gdansk, Poland, Olivia launched the charitable cause “Skating for Young Heroes”. Working in conjunction with the Polish children’s charity Fundacja Dziecięca Fantazja, “Skating for Young Heroes” brings joy and love to children, aged 3 - 18, who have a terminal illness or life-limiting condition. Olivia and Josh visited the homes of 9 terminally-ill children in Poland to spend time with the children and to make their dreams come true by presenting them with their material wish gifts including: laptops, smartphones, gaming consoles, bedroom furniture, autographed sports and entertainment memorabilia, toys and games.
                                                                                <div style="color: black !important;">

                                                                                    <p>In December, 2019, through “Skating for Young Heroes”, Olivia and Josh were guests of honor at a Gala Christmas Party in Warsaw, Poland, for over 300 sick children and handed out presents to all the children. The event is the largest of its kind in Europe.
                                                                                    <div style="color: black !important;">Olivia combines her full time training and competition schedule with her modeling and humanitarian work whilst excelling with her high school education.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="float: left;">&nbsp;</div>
					</div> <!-- end #entry-content -->
				</article>
			</main> <!-- end #main -->
		</div> <!-- #primary -->
	</div><!-- end .wrap -->
            ';
        }
    ?>

</div><!-- end .about-content -->
<?php get_footer();
