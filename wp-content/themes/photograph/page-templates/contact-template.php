<?php
/**
 * Template Name: Contact Template
 *
 * Displays the contact page template.
 *
 * @package Theme Freesia
 * @subpackage Photograph
 * @since Photograph 1.0
 */
get_header();
	global $photograph_settings;
	$photograph_settings = wp_parse_args(  get_option( 'photograph_theme_options', array() ),  photograph_get_option_defaults_values() );
	if( $post ) {
		$layout = get_post_meta( get_queried_object_id(), 'photograph_sidebarlayout', true );
	}
	if( empty( $layout ) || is_archive() || is_search() || is_home() ) {
		$layout = 'default';
	}
$attachment_id = get_post_thumbnail_id();
$image_attributes = wp_get_attachment_image_src($attachment_id,'full'); ?>
<div <?php post_class('contact-content'); if(has_post_thumbnail()){ ?> style="background-image:url('<?php echo esc_url($image_attributes[0]); ?>');" <?php } ?>>
    <div id="content" class="site-content">

        <?php
        if (isMobile())
        {
echo '        <div class="contact-content post-10 page type-page status-publish has-post-thumbnail hentry" style="background:white");">
';
        }
        else{
            echo '        <div class="contact-content post-10 page type-page status-publish has-post-thumbnail hentry" style="background-image:url(\''.get_bloginfo('wpurl').'/wp-content/'.wp_basename( $uploads['baseurl'] ).'/2020/03/bg-2-scaled.jpg\');">
';
        }
        ?>
            <div class="wrap">
                  <?php
        if (isMobile())
        {
                echo '<div id="primary" class="content-area" style="width: 100% !important; margin-top: -35% !important;">';
                }
        else{
            echo '                <div id="primary"  class="content-area">
';
        }
        ?>
                    <main id="main" class="site-main" role="main">
                        <article>
                            <header class="page-header">
                                <h1 class="page-title">Contact</h1>
                                <!-- .page-title -->
                                <!-- .breadcrumb -->
                            </header><!-- .page-header -->
                            <div class="page-content clearfix">
                                <div role="form" class="wpcf7" id="wpcf7-f170-p10-o1" lang="pl-PL" dir="ltr">
                                    <div class="screen-reader-response"></div>
                                    <form action="/contact/#wpcf7-f170-p10-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                        <div style="display: none;">
                                            <input type="hidden" name="_wpcf7" value="170">
                                            <input type="hidden" name="_wpcf7_version" value="5.1.6">
                                            <input type="hidden" name="_wpcf7_locale" value="pl_PL">
                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f170-p10-o1">
                                            <input type="hidden" name="_wpcf7_container_post" value="10">
                                        </div>
                                        <p><label> Your Name (required)<br>
                                                <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </label></p>
                                        <p><label> Your Email (required)<br>
                                                <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </label></p>
                                        <p><label> Subject<br>
                                                <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span> </label></p>
                                        <p><label> Your Message<br>
                                                <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </label></p>
                                        <p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span></p>
                                        <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>

                                <div id="comments" class="comments-area">
                                </div> <!-- .comments-area -->					</div> <!-- end #page-content -->
                        </article>
                    </main> <!-- end #main -->
                </div> <!-- #primary -->

            </div><!-- end .wrap -->

        </div><!-- end .contact-content -->
    </div>
	<?php if ( is_active_sidebar( 'photograph_form_for_contact_page' ) ) : ?>
		
	<?php endif;  ?>
</div><!-- end .contact-content -->
<?php
get_footer();
