wp.Modula = 'undefined' === typeof( wp.Modula ) ? {} : wp.Modula;

jQuery( document ).ready( function( $ ){

	// Initialize Filters
	wp.Modula.Filters = new wp.Modula.models['filters']({
		'filters' : wp.Modula.Settings.get( 'filters' ),
	});

	// Bulk Edit
	wp.Modula.BulkEdit = new wp.Modula.bulkedit['model'];

	$('#modula-bulk-edit').click(function( evt ){
		evt.preventDefault();
		wp.Modula.BulkEdit.open();
	});

	if ( 'undefined' != typeof wp.Modula.Settings ) {
		var maxImageCount = $( '#modula-general tr[data-container="maxImagesCount"]' );

		wp.Modula.Settings.on( 'change:type', function( event, type ){
			if ( 'custom-grid' == type ) {
				maxImageCount.hide();
			}else{
				maxImageCount.show();
			}
		});

		if ( wp.Modula.Settings.get('type') == 'custom-grid' ) {
			maxImageCount.hide();
		}

	}

});