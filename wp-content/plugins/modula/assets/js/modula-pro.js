function modula_pro_get_url_parameters(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}

jQuery( document ).on( 'modula_api_after_init', function( event, data ){
	var lightboxes = {
			'magnific' : {
				'options' : { type: 'image', image: { titleSrc: 'data-title' }, gallery: { enabled: true }, delegate: 'a.tile-inner' }
			},
			'prettyphoto' : {
				'options' : { social_tools: '' }
			},
			'fancybox' : {
				'options' : { loop: true }
			},
			'swipebox' : {
				'options' : { loopAtEnd: true }
			},
			'lightgallery' : {
				'options' : { selector: 'a.tile-inner' }
			}
		},
		currentLightbox, filters;
	
	// Check what lightbox we need to use and initialize it.
	if ( 'undefined' != typeof lightboxes[ data.options.lightbox ] ) {
		currentLightbox = lightboxes[ data.options.lightbox ];

		if ( 'magnific' == data.options.lightbox && 'function' == typeof jQuery.fn['magnificPopup'] ) {
			jQuery( data.element ).magnificPopup( currentLightbox['options'] );
		}else if ( 'prettyphoto' == data.options.lightbox && 'function' == typeof jQuery.fn['prettyPhoto'] ) {
			jQuery( data.element ).find( 'a.tile-inner' ).prettyPhoto( currentLightbox['options'] );
		}else if ( 'fancybox' == data.options.lightbox && 'function' == typeof jQuery.fn['fancybox'] ) {
			jQuery( data.element ).find( 'a.tile-inner' ).fancybox( currentLightbox['options'] );
		}else if ( 'swipebox' == data.options.lightbox && 'function' == typeof jQuery.fn['swipebox'] ) {
			jQuery( data.element ).find( 'a.tile-inner' ).swipebox( currentLightbox['options'] );
		}else if ( 'lightgallery' == data.options.lightbox && 'function' == typeof jQuery.fn['lightGallery'] ) {
			jQuery( data.element ).lightGallery( currentLightbox['options'] );
		}
	}

	// Check if we have filters and initialize filters.
	filters = jQuery( data.element ).find( '.filters > a' );

	if ( filters.length > 0 ) {
		var filterClick = data.options.filterClick;

		// Check url to see if we have a filter
		var urlFilter = modula_pro_get_url_parameters( 'jtg-filter' );

		if ( urlFilter ) {
			var currentFilter = 'jtg-filter-' + urlFilter;

			// Show filtered items
			data.$items.removeClass('jtg-hidden');
            data.$items.show();
            
            // If we need to show all, don't hide anything
            if ( 'all' != urlFilter ) {
            	data.$items.not("." + currentFilter ).addClass( "jtg-hidden" ).hide();
            }

            data.reset();
		}

	    data.$element.on( "click", ".filters a", function (e) {

	        if( '0' == filterClick ) {
	            e.preventDefault();
	        }
			
			if( jQuery(this).hasClass("selected") ){
				return;
			}
				
			data.$element.find(".filters a").removeClass("selected");
			jQuery(this).addClass("selected");
			
	        var filter = jQuery(this).attr("href").substr(1);

	        if ( filter ) {
	            data.$items.removeClass('jtg-hidden');
	            data.$items.show();
	            data.$items.not("." + filter).addClass("jtg-hidden").hide();                
	        } else {
	            data.$items.removeClass('jtg-hidden');
	            data.$items.show();
	        }

	        data.reset();
	    });
	}
	

});