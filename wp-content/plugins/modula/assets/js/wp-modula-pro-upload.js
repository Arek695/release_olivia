wp.Modula = 'undefined' === typeof( wp.Modula ) ? {} : wp.Modula;
wp.Modula.upload = 'undefined' === typeof( wp.Modula.upload ) ? {} : wp.Modula.upload;

(function( $, modula ){

	var ModulaPROSelection = modula.upload['selection'].extend({
		add: function( models, options ) {

			console.log( 'ModulaPROSelection' );

			/**
			 * call 'add' directly on the parent class
			 */
			return wp.media.model.Attachments.prototype.add.call( this, models, options );
		},
	});

	var uploadPRoHandler = modula.upload['uploadHandler'].extend({
		fileupload: function( up, file, info ){

			var modulaGalleryObject = this,
				response = JSON.parse( info.response );

			modulaGalleryObject.generateSingleImage( response['data'] );

		},
	});

	modula.upload['selection']     = ModulaPROSelection;
	modula.upload['uploadHandler'] = uploadPRoHandler;

}( jQuery, wp.Modula ))