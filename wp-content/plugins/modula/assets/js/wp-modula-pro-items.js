wp.Modula = 'undefined' === typeof( wp.Modula ) ? {} : wp.Modula;
wp.Modula.items = 'undefined' === typeof( wp.Modula.items ) ? {} : wp.Modula.items;

(function( $, modula ){

	var modulaProItemsCollection = modula.items['collection'].extend({

		addItem: function( model ) {
            this.add( model );
        }

    });

	modula.items['collection'] = modulaProItemsCollection;

}( jQuery, wp.Modula ))