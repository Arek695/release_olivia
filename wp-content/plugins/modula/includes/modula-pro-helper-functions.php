<?php

/* Modula PRO Filter Functions */

// Function to output all filters
function modula_pro_output_filters( $settings ) {

	if ( ! isset( $settings['filters'] ) ) {
		return '';
	}

	$filters = Modula_Pro_Helper::remove_empty_items( $settings['filters'] );

	if ( ! is_array( $filters ) ) {
		return;
	}

	if ( empty( $filters ) ) {
		return;
	}

	$current_filter = isset($_GET['jtg-filter']) ? $_GET['jtg-filter'] : 'all';
	$filter_url = $settings['filterClick']  ? '?jtg-filter=all' : '#';

	echo "<div class='filters'>";
	echo '<a data-filter="all" href="' . $filter_url . '" class="' . ( 'all' == $current_filter ? 'selected' : '' ) . '">' . $settings['allFilterLabel'] . '</a>';
	foreach( $filters as $filter ) {
		$filter_slug = sanitize_title( $filter );
		$filter_url = $settings['filterClick'] ? '?jtg-filter=' . $filter_slug : "#jtg-filter-". $filter_slug;
		echo '<a data-filter="' . $filter_slug . '" href="' . $filter_url . '" class="' . ( $current_filter == $filter_slug ? 'selected' : '' ) . '">' . $filter . '</a>';
	}
	echo "</div>";

}

// Add filters to items
function modula_pro_add_filters( $item_data, $item, $settings ) {

	if ( isset( $item['filters'] ) ) {
		$filters = explode( ',', $item['filters'] );
		foreach ( $filters as $filter ) {
			$item_data['item_classes'][] = 'jtg-filter-' . sanitize_title( $filter );
		}
	}

	return $item_data;

}

// Add extra attributes for facybox
function modula_pro_fancybox( $item_data, $item, $settings ) {

	if ( 'fancybox' == $settings['lightbox'] ) {
		$item_data['link_attributes']['data-fancybox'] = $settings['gallery_id'];
	}

	return $item_data;
}
