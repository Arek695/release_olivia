<?php

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * @since      2.0.0
 */
class Modula_PRO {
	
	function __construct() {
		
		$this->load_dependencies();

		add_action( 'wp_enqueue_scripts', array( $this, 'register_gallery_scripts' ) );

		// Enqueue scripts for selected lightbox
		add_action( 'modula_lighbox_shortcode', array( $this, 'enqueue_selected_lightbox_scripts' ) );

		add_filter( 'modula_necessary_scripts', array( $this, 'enqueue_necessary_scripts' ) );
		add_filter( 'modula_necessary_styles', array( $this, 'modula_necessary_styles' ) );

		// Modify Modula Gallery config
		add_filter( 'modula_gallery_settings', array( $this, 'modula_pro_config' ), 10, 2 );

		add_action( 'modula_shortcode_before_items', 'modula_pro_output_filters', 15 );
		add_filter( 'modula_shortcode_item_data', 'modula_pro_add_filters', 30, 3 );
		add_filter( 'modula_shortcode_item_data', 'modula_pro_fancybox', 40, 3 );
		add_filter( 'modula_gallery_images', array( $this, 'modula_pro_max_count' ), 10, 2 );

		// Modify CSS
		add_filter( 'modula_shortcode_css', array( $this, 'generate_new_css' ), 10, 3 );

		// Remove upsells
		add_filter( 'modula_show_upsells', '__return_false' );

		if ( is_admin() ) {
			$this->check_for_lite();
		}

	}

	private function load_dependencies() {

		require_once MODULA_PRO_PATH . 'includes/modula-pro-helper-functions.php';
		require_once MODULA_PRO_PATH . 'includes/class-modula-pro-helper.php';
		require_once MODULA_PRO_PATH . 'includes/admin/class-modula-pro-settings.php';

		if ( is_admin() ) {
			require_once MODULA_PRO_PATH . 'includes/admin/class-modula-pro-addon.php';
			require_once MODULA_PRO_PATH . 'includes/admin/modula-pro-addon-ajax.php';
		}

	}

	// Register all pro scripts & style in order to be enqueue
	public function register_gallery_scripts() {

		// Register fancybox
		wp_register_style( 'fancybox', MODULA_PRO_URL . 'assets/lightboxes/fancybox/jquery.fancybox.min.css', MODULA_PRO_VERSION, null );
		wp_register_script( 'fancybox', MODULA_PRO_URL . 'assets/lightboxes/fancybox/jquery.fancybox.min.js', array( 'jquery' ), MODULA_PRO_VERSION, true );

		// Register lightgallery
		wp_register_style( 'lightgallery', MODULA_PRO_URL . 'assets/lightboxes/lightgallery/css/lightgallery.min.css', MODULA_PRO_VERSION, null );
		wp_register_script( 'lightgallery', MODULA_PRO_URL . 'assets/lightboxes/lightgallery/js/lightgallery.min.js', array( 'jquery' ), MODULA_PRO_VERSION, true );

		// Register magnific popup
		wp_register_style( 'magnific-popup', MODULA_PRO_URL . 'assets/lightboxes/magnific/magnific-popup.css', MODULA_PRO_VERSION, null );
		wp_register_script( 'magnific-popup', MODULA_PRO_URL . 'assets/lightboxes/magnific/jquery.magnific-popup.min.js', array( 'jquery' ), MODULA_PRO_VERSION, true );

		// Register prettyphoto
		wp_register_style( 'prettyphoto', MODULA_PRO_URL . 'assets/lightboxes/prettyphoto/style.css', MODULA_PRO_VERSION, null );
		wp_register_script( 'prettyphoto', MODULA_PRO_URL . 'assets/lightboxes/prettyphoto/script.js', array( 'jquery' ), MODULA_PRO_VERSION, true );

		// Register swipebox
		wp_register_style( 'swipebox', MODULA_PRO_URL . 'assets/lightboxes/swipebox/css/swipebox.min.css', MODULA_PRO_VERSION, null );
		wp_register_script( 'swipebox', MODULA_PRO_URL . 'assets/lightboxes/swipebox/js/jquery.swipebox.min.js', array( 'jquery' ), MODULA_PRO_VERSION, true );

		// Modula PRO
		wp_register_style( 'modula-pro-effects', MODULA_PRO_URL . 'assets/css/effects.css', MODULA_PRO_VERSION, null );
		wp_register_script( 'modula-pro', MODULA_PRO_URL . 'assets/js/modula-pro.js', array( 'jquery' ), MODULA_PRO_VERSION, true );

	}

	// Enqueue specific styles & scripts for each lightbox
	public function enqueue_selected_lightbox_scripts( $lightbox ) {

		switch ( $lightbox ) {
			case 'fancybox':
				wp_enqueue_style( 'fancybox' );
				wp_enqueue_script( 'fancybox' );
				break;
			case 'lightgallery':
				wp_enqueue_style( 'lightgallery' );
				wp_enqueue_script( 'lightgallery' );
				break;
			case 'magnific':
				wp_enqueue_style( 'magnific-popup' );
				wp_enqueue_script( 'magnific-popup' );
				break;
			case 'prettyphoto':
				wp_enqueue_style( 'prettyphoto' );
				wp_enqueue_script( 'prettyphoto' );
				break;
			case 'swipebox':
				wp_enqueue_style( 'swipebox' );
				wp_enqueue_script( 'swipebox' );
				break;
		}

	}

	// Add extra scripts for shortcode to enqueue
	public function enqueue_necessary_scripts( $scripts ) {

		$scripts[] = 'modula-pro';
		return $scripts;

	}

	// Add extra css for shortcode to enqueue
	public function modula_necessary_styles( $styles ) {

		// Search for css for effect in lite and remove it.
		$lite_effects = array_search( 'modula-effects', $styles );
		if ( false !== $lite_effects ) {
			unset( $styles[ $lite_effects ] );
		}

		$styles[] = 'modula-pro';
		$styles[] = 'modula-pro-effects';
		return $styles;

	}

	// Add extra parameter for javascript config
	public function modula_pro_config( $js_config, $settings ) {
		$js_config['lightbox']    = $settings['lightbox'];

		if ( isset( $settings['filterClick'] ) ) {
			$js_config['filterClick'] = $settings['filterClick'];
		}
		
		return $js_config;
	}

	public function generate_new_css( $css, $gallery_id, $settings ){

		$css .= "#{$gallery_id} .item { background-color:". $settings['hoverColor'] ."; }";

		if ( absint( $settings['hoverOpacity'] ) <= 100 ) {
			$css .= "#{$gallery_id} .item:hover img { opacity: " . ( 1 - absint( $settings['hoverOpacity'] ) / 100 ) . "; }";
		}

		$css .= "#{$gallery_id} .item { transform: scale(" . $settings['loadedScale'] / 100 . ") translate(" . $settings['loadedHSlide'] . 'px,' . $settings['loadedVSlide'] . "px) rotate(" . $settings['loadedRotate'] . "deg); }";
		
		return $css;
	}

	// Check if Modula Lite is Active
	private function check_for_lite() {

		$check = array(
			'installed' => Modula_Pro_Helper::check_plugin_is_installed( 'modula-best-grid-gallery' ),
			'active'    => Modula_Pro_Helper::check_plugin_is_active( 'modula-best-grid-gallery' ),
		);

		if ( $check['active'] ) {
			return;
		}

		add_action( 'admin_notices', array( $this, 'display_lite_notice' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_lite_scripts' ) );

	}

	public function display_lite_notice() {

		$check = array(
			'installed' => Modula_Pro_Helper::check_plugin_is_installed( 'modula-best-grid-gallery' ),
			'active'    => Modula_Pro_Helper::check_plugin_is_active( 'modula-best-grid-gallery' ),
		);

		if ( ! $check['installed'] ) {
			$label  = esc_html__( 'Install & Active: Modula Lite', 'modula' );
			$action = 'install';
			$url = '#';
		}else{
			$label  = esc_html__( 'Activate: Modula Lite', 'modula' );
			$action = 'activate';
			$url = add_query_arg(
				array(
					'action'        => 'activate',
					'plugin'        => rawurlencode( Modula_Pro_Helper::_get_plugin_basename_from_slug( 'modula-best-grid-gallery' ) ),
					'plugin_status' => 'all',
					'paged'         => '1',
					'_wpnonce'      => wp_create_nonce( 'activate-plugin_' . Modula_Pro_Helper::_get_plugin_basename_from_slug( 'modula-best-grid-gallery' ) ),
				),
				admin_url( 'plugins.php' )
			);
		}

		echo '<div class="notice" style="background: #e9eff3;padding: 60px;border: 10px solid #fff;text-align:center;">';
		echo '<h1 style="text-align:center">' . esc_html__( 'Install & Activate Modula Lite', 'modula' ) . '</h1>';
		echo '<h4 style="text-align:center;">' . esc_html__( 'Since version 2.0.0 in order for Modula PRO to work properly, you\'ll also need to have Modula Lite installed & activated', 'modula' ) . '</h4>';
		echo '<a href="' . $url . '" data-action="' . $action . '" class="button button-primary button-hero" id="install-modula-lite" style="line-height: 23px;padding: 12px 36px;">' . $label . '</a>';
		echo '</div>';

	}

	public function admin_lite_scripts() {
		wp_enqueue_script( 'modula-install-lite', MODULA_PRO_URL . 'assets/js/install-lite.js', array( 'jquery', 'updates' ), null, true );
	}

	public function modula_pro_max_count( $images, $settings ) {

		if ( isset( $settings['maxImagesCount'] ) && absint( $settings['maxImagesCount'] ) > 0 && 'creative-gallery' == $settings['type'] ) {
			$images = array_slice( $images, 0, absint( $settings['maxImagesCount'] ) );
		}

		return $images;
	}

}