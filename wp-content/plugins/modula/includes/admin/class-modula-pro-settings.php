<?php

/**
 * 
 */
class Modula_PRO_Settings {
	
	function __construct() {

		// New CSS & JS for PRO version
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
		add_action( 'modula_scripts_before_wp_modula', array( $this, 'modula_pro_backbone' ) );
		add_action( 'modula_scripts_after_wp_modula', array( $this, 'modula_pro_main' ) );
		
		// Filter Modula Tabs
		add_filter( 'modula_gallery_tabs', array( $this, 'modula_pro_tabs' ) );

		// Filter Modula Fields
		add_filter( 'modula_gallery_fields', array( $this, 'modula_pro_fields' ) );

		// Flter Hover Effects
		add_filter( 'modula_available_hover_effects', array( $this, 'modula_pro_hover_effects' ) );
		add_filter( 'modula_pro_hover_effects', '__return_false' );

		// Create preview of new hover effects
		add_filter( 'modula_hover_effect_preview', array( $this, 'modula_pro_hover_effect_preview' ), 10, 2 );

		// Create filters field
		add_filter( 'modula_render_filters_field_type', array( $this, 'modula_pro_filters_field' ), 10, 3 );

		// Filter Defaults
		add_filter( 'modula_lite_default_settings', array( $this, 'default_settings' ) );

		// Save Filters for our items
		add_filter( 'modula_gallery_image_attributes', array( $this, 'add_pro_item_fields' ) );

		/* Add templates for our plugin */
		add_action( 'admin_footer', array( $this, 'print_modula_pro_templates' ) );

		// Add new input for item
		add_action( 'modula_item_extra_fields', array( $this, 'extra_item_fields' ) );

		// Add license tab
		add_filter( 'modula_admin_page_tabs', array( $this, 'add_license_tab' ) );

		/* Show pro vs lite tab content */
		add_action( 'modula_admin_tab_licenses', array( $this, 'show_licenses_tab' ) );

		/* Add values for sanitizations */
		add_filter( 'modula_lightbox_values', array( $this, 'add_lightboxes_pro' ) );
		add_filter( 'modula_effect_values', array( $this, 'add_effects_pro' ) );

		// Sanitize filter's fields
		add_filter( 'modula_settings_field_sanitization', array( $this, 'sanitize_settings' ), 20, 4 );
		add_filter( 'modula_image_field_sanitization', array( $this, 'sanitize_image_fields' ), 20, 3 );

		// Add Bulk edit button
		add_action( 'modula_after_helper_grid', array( $this, 'show_bulk_edit_button' ) );

		// Render FIeld Type
		add_filter( 'modula_render_content_field_type', array( $this, 'render_field_type' ), 10, 3 );

		// Change Field Type Format
		add_filter( 'modula_field_type_content_format', array( $this, 'content_format' ), 10, 2 );

	}

	public function admin_scripts( $hook ){

		global $id, $post;

        // Get current screen.
        $screen = get_current_screen();

        // Check if is modula custom post type
        if ( 'modula-gallery' !== $screen->post_type ) {
            return;
        }

        // Set the post_id
        $post_id = isset( $post->ID ) ? $post->ID : (int) $id;

		if ( 'post-new.php' == $hook || 'post.php' == $hook ) {

			// Modula PRO hover effects
			wp_enqueue_style( 'modula-pro-effects', MODULA_PRO_URL . 'assets/css/effects.css' );
			wp_enqueue_style( 'modula-pro-selectize', MODULA_PRO_URL . 'assets/css/libraries/selectize/selectize.css' );
			wp_enqueue_style( 'modula-pro-selectize-default', MODULA_PRO_URL . 'assets/css/libraries/selectize/selectize.default.css' );
			wp_enqueue_style( 'modula-pro-style', MODULA_PRO_URL . 'assets/css/modula-pro-admin-style.css' );

		}

	}

	public function modula_pro_backbone() {

		// Modula PRO effects
		wp_enqueue_script( 'modula-pro-selectize', MODULA_PRO_URL . 'assets/js/libraries/selectize/selectize.min.js', array( 'jquery' ), MODULA_PRO_VERSION, true );
		wp_enqueue_script( 'modula-pro-filters', MODULA_PRO_URL . 'assets/js/wp-modula-filters.js', array( 'jquery', 'jquery-ui-sortable' ), MODULA_PRO_VERSION, true );
		wp_enqueue_script( 'modula-pro-items', MODULA_PRO_URL . 'assets/js/wp-modula-pro-items.js', array( 'jquery', 'jquery-ui-sortable' ), MODULA_PRO_VERSION, true );
		wp_enqueue_script( 'modula-pro-upload', MODULA_PRO_URL . 'assets/js/wp-modula-pro-upload.js', array( 'jquery', 'jquery-ui-sortable' ), MODULA_PRO_VERSION, true );
		wp_enqueue_script( 'modula-pro-bulkedit', MODULA_PRO_URL . 'assets/js/wp-modula-bulkedit.js', array( 'jquery', 'jquery-ui-sortable' ), MODULA_PRO_VERSION, true );

	}

	public function modula_pro_main() {

		// Modula PRO main JS
		wp_enqueue_script( 'modula-pro', MODULA_PRO_URL . 'assets/js/wp-modula-pro.js', array( 'jquery' ), MODULA_PRO_VERSION, true );
		
	}

	// Modula PRO Tabs
	public function modula_pro_tabs( $tabs ){

		if ( ! isset( $tabs['filters'] ) ) {
			$tabs['filters'] = array(
				'label'    => esc_html__( 'Filters', 'modula-pro-gallery' ),
				"icon"     => "dashicons dashicons-filter",
				'priority' => 15,
			);
		}
		

		return $tabs;
	}

	// Modula PRO Fields
	public function modula_pro_fields( $fields ){

		// Remove restrictions on lightboxes
		if ( isset( $fields['general']['lightbox'] ) ) {
			
			// Remove disabled lightboxes
			if ( isset( $fields['general']['lightbox']['disabled'] ) ) {
				unset( $fields['general']['lightbox']['disabled'] );
			}

			// Add all lightboxes
			if ( $fields['general']['lightbox']['values'][ esc_html__( 'Lightboxes', 'modula-gallery' ) ] ) {
				$fields['general']['lightbox']['values'][ esc_html__( 'Lightboxes', 'modula-gallery' ) ] = array(
					"magnific"     => esc_html__( 'Magnific popup', 'modula-gallery' ),
					"prettyphoto"  => esc_html__( 'PrettyPhoto', 'modula-gallery' ),
					"fancybox"     => esc_html__( 'FancyBox', 'modula-gallery' ),
					"swipebox"     => esc_html__( 'SwipeBox', 'modula-gallery' ),
					"lightbox2"    => esc_html__( 'Lightbox', 'modula-gallery' ),
					"lightgallery" => esc_html__( 'LightGallery', 'modula-gallery' ),
				);
			}

		}

		// Add Max Image field
		$fields['general']['maxImagesCount'] = array(
			"name"        => esc_html__( 'Max Images Count', 'modula-gallery' ),
			"type"        => "text",
			"default"     => 0,
			"description" => esc_html__( 'Use 0 to show all images', 'modula-gallery' ),
		);

		// Add filters settings
		$fields['filters'] = array(
			'filters' => array(
				"name"     => esc_html__( 'Filters', 'modula-gallery' ),
				"type"     => "filters",
				'priority' => 10,
			),
			'filterClick' => array(
				"name"        => esc_html__( 'Reload Page On Filter Click', 'modula-gallery' ),
				"type"        => "toggle",
				"default"     => 0,
				"description" => esc_html__( 'Choose whether pages reload to sort images when a filter is clicked.', 'modula-gallery' ),
				'priority'    => 20,
			),
			'allFilterLabel' => array(
				"name"        => esc_html__( 'Text For "All" Filter', 'modula-gallery' ),
				"type"        => "text",
				'default'     => esc_html__( 'All', 'modula-gallery' ),
				"description" => esc_html__( 'Set the label you wish to use for the ‘All’ filter that will contain all of your gallery’s images.', 'modula-gallery' ),
				'priority'    => 30,
			),
		);

		// Add image loaded effects
		$fields['image-loaded-effects']['loadedRotate'] = array(
			"name"        => esc_html__( 'Rotate', 'modula-gallery' ),
			"description" => esc_html__( 'To add a rotate effect set this value to negative for counter-clockwise rotations and positive for clockwise rotations.', 'modula-gallery' ),
			"type"        => "ui-slider",
			"min"         => -180,
			"max"         => 180,
			"default"     => 0,
			'priority'    => 20,
		);
		$fields['image-loaded-effects']['loadedHSlide'] = array(
			"name"        => esc_html__( 'Horizontal Slide', 'modula-gallery' ),
			"description" => esc_html__( 'Set this to a negative value if you want your gallery’s image to slide in from the left or to positive if you would prefer for them to slide in from the right.', 'modula-gallery' ),
			"type"        => "ui-slider",
			"min"         => -100,
			"max"         => 100,
			"default"     => 0,
			'priority'    => 30,
		);
		$fields['image-loaded-effects']['loadedVSlide'] = array(
			"name"        => esc_html__( 'Vertical Slide', 'modula-gallery' ),
			"description" => esc_html__( 'Set this to a negative value if you want your gallery’s image to slide in from the top or to positive if you would prefer for them to slide in from the bottom.', 'modula-gallery' ),
			"type"        => "ui-slider",
			"min"         => -100,
			"max"         => 100,
			"default"     => 0,
			'priority'    => 40,
		);

		// Hover Effects
		$fields['hover-effect']['hoverColor'] = array(
			"name"        => esc_html__( 'Hover Color', 'modula-gallery' ),
			"type"        => "color",
			"description" => '',
			"default"     => "#ffffff",
			'priority'    => 20,
		);
		$fields['hover-effect']['hoverOpacity'] = array(
			"name"        => esc_html__( 'Hover Opacity', 'modula-gallery' ),
			"description" => esc_html__( 'Adjust the transparency of your chosen hover effect.', 'modula-gallery' ),
			"type"        => "ui-slider",
			"min"         => 0,
			"max"         => 100,
			"default"     => 50,
			'priority'    => 30,
		);

		if ( ! function_exists( 'is_plugin_active' ) ) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		// Check if modula video it's installed & activated
		if ( ! is_plugin_active( 'modula-video/modula-video.php' ) ) {
			$fields['video']['helper-message'] = array(
				"name"     => ' ',
				"type"     => "content",
				"content"  => sprintf( esc_html__( 'In order to add videos to your gallery you\'ll need to install the extension Modula Video from %shere%s.', 'modula-gallery' ), '<a href="' . admin_url( 'edit.php?post_type=modula-gallery&page=modula-addons' ) . '" target="blank">', '</a>' ),
				'priority' => 5,
			);
		}
		

		// Check if modula speed up it's installed & activated
		if ( ! is_plugin_active( 'modula-speedup/modula-speedup.php' ) ) {
			$fields['speedup']['helper-message'] = array(
				"name"     => ' ',
				"type"     => "content",
				"content"  => sprintf( esc_html__( 'In order to make your gallery load faster you\'ll need to install the extension Modula Speed Up from %shere%s.', 'modula-gallery' ), '<a href="' . admin_url( 'edit.php?post_type=modula-gallery&page=modula-addons' ) . '" target="blank">', '</a>' ),
				'priority' => 5,
			);
		}

		return $fields;
	}

	// Add new hover effects in pro version
	public function modula_pro_hover_effects( $effects ) {

		return array(
			'none'      => esc_html__( 'None', 'modula-gallery' ),
			'pufrobo'   => esc_html__( 'Pufrobo', 'modula-gallery' ),
			'fluid-up'  => esc_html__( 'Fluid Up', 'modula-gallery' ),
			'hide'      => esc_html__( 'Hide', 'modula-gallery' ),
			'quiet'     => esc_html__( 'Quiet', 'modula-gallery' ),
			'catinelle' => esc_html__( 'Catinelle', 'modula-gallery' ),
			'reflex'    => esc_html__( 'Reflex', 'modula-gallery' ),
			'curtain'   => esc_html__( 'Curtain', 'modula-gallery' ),
			'lens'      => esc_html__( 'Lens', 'modula-gallery' ),
			'appear'    => esc_html__( 'Appear', 'modula-gallery' ),
			'crafty'    => esc_html__( 'Crafty', 'modula-gallery' ),
			'seemo'     => esc_html__( 'Seemo', 'modula-gallery' ),
			'comodo'    => esc_html__( 'Comodo', 'modula-gallery' ),
		);

	}

	// Generate hover effect preview html
	public function modula_pro_hover_effect_preview( $preview_html, $effect ) {

		$effect_elements = Modula_Helper::hover_effects_elements( $effect );

		$html = '';
		$html .= '<div class="panel panel-' . $effect . ' items clearfix">';
		$html .= '<div class="item effect-' . $effect . '">';
		$html .= '<img src="' . MODULA_URL . '/assets/images/effect.jpg" class="pic">';
		$html .= '<div class="figc"><div class="figc-inner">';
		if ( $effect_elements['title'] ) {
			$html .= '<h2>Lorem ipsum</h2>';
		}
		if ( $effect_elements['description'] ) {
			$html .= '<p class="description">Quisque diam erat, mollisvitae enim eget</p>';
		}else{
			$html .= '<p class="description"></p>';
		}
		if ( $effect_elements['social'] ) {
			$html .= '<div class="jtg-social">';
			$html .= '<a class="fa fa-twitter" href="#">' . Modula_Helper::get_icon( 'twitter' ) . '</a>';
			$html .= '<a class="fa fa-facebook" href="#">' . Modula_Helper::get_icon( 'facebook' ) . '</a>';
			$html .= '<a class="fa fa-google-plus" href="#">' . Modula_Helper::get_icon( 'google' ) . '</a>';
			$html .= '<a class="fa fa-pinterest" href="#">' . Modula_Helper::get_icon( 'pinterest' ) . '</a>';
			$html .= '</div>';
		}
		$html .= '</div></div></div>';
		$html .= '<div class="effect-compatibility">';
		$html .= '<p class="description">' . esc_html__( 'This effect is compatible with:', 'modula-gallery' );

		if ( $effect_elements['title'] ) {
			$html .= '<span><strong> ' . esc_html__( 'Title', 'modula-gallery' ) . '</strong></span>,';
		}

		if ( $effect_elements['description'] ) {
			$html .= '<span><strong> ' . esc_html__( 'Description', 'modula-gallery' ) . '</strong></span>,';
		}

		if ( $effect_elements['social'] ) {
			$html .= '<span><strong> ' . esc_html__( 'Social Icons', 'modula-gallery' ) . '</strong></span></p>';
		}
		
		$html .= '</div>';
		$html .= '</div>';

		return $html;

	}

	public function modula_pro_filters_field( $html, $field, $value ){

		$html = '<div id="modula-filters" class="modula-filters-container">';
		$html .= '<div class="modula-filters">';

		if ( ! is_array( $value ) ) {
			$value = explode( '|', $value );
		}

		if ( empty( $value ) ) {
			$html .= '<div class="modula-filter-input"><span class="dashicons dashicons-move"></span><input type="text" name="modula-settings[' . $field['id'] . '][]" value=""><a href="#" class="modula-delete-filter"><span class="dashicons dashicons-trash"></span></a></div>';
		}else{

			foreach ( $value as $filter ) {
				$html .= '<div class="modula-filter-input"><span class="dashicons dashicons-move"></span><input type="text" name="modula-settings[' . $field['id'] . '][]" value="' . $filter . '" class="regular-text"><a href="#" class="modula-delete-filter"><span class="dashicons dashicons-trash"></span></a></div>';
			}

		}

		$html .= '</div>';
		$html .= '<a href="#" id="modula-add-filter" class="button" data-field-name="' . $field['id'] . '"><span class="dashicons dashicons-plus"></span>' . esc_html__( 'Add new filter', 'modula' ) . '</a>';
		$html .= '</div>';

		return $html;

	}

	public function default_settings( $defaults ){

		$defaults['maxImagesCount'] = 0;
		$defaults['filters'] = array( '' );
		$defaults['filterClick'] = 0;
		$defaults['allFilterLabel'] = esc_html__( 'All', 'modula-gallery' );
		$defaults['loadedRotate'] = 0;
		$defaults['loadedHSlide'] = 0;
		$defaults['loadedVSlide'] = 0;
		$defaults['hoverColor'] = '#fff';
		$defaults['hoverOpacity'] = 50;

		return $defaults;

	}

	public function print_modula_pro_templates(){
		include 'modula-pro-js-templates.php';
	}

	public function extra_item_fields() {
		echo '<input type="hidden" name="modula-images[filters][{{data.index}}]" class="modula-image-filters" value="{{ data.filters }}">';
	}

	public function add_pro_item_fields( $fields ) {

		$fields[] = 'filters';
		return $fields;

	}

	public function add_license_tab( $tabs ){

		$tabs['licenses'] = array(
			'label'    => esc_html__( 'Licenses', 'modula' ),
			'priority' => 30
		);

		return $tabs;

	}

	public function show_licenses_tab() {
		include 'tabs/license.php';
	}

	public function add_lightboxes_pro( $values ){

		$values = array_merge( $values, array( "magnific", "prettyphoto", "fancybox", "swipebox", "lightbox2", "lightgallery" ) );
		return $values;
	}

	public function add_effects_pro( $values ){
		
		$values = array_merge( $values, array( 'fluid-up', 'hide', 'quiet', 'catinelle', 'reflex', 'curtain', 'lens', 'appear', 'crafty', 'seemo', 'comodo' ) );
		return $values;
	}

	public function sanitize_settings( $sanitized_value, $value, $field_id, $field ){

		if ( 'filters' == $field_id ) {
			$new_value = array();
			if ( is_array( $value ) && ! empty( $value ) ) {
				foreach ( $value as $filter ) {
					$new_value[] = sanitize_text_field( $filter );
				}
			}

			return $new_value;
		}

		return $sanitized_value;

	}

	public function sanitize_image_fields( $sanitized_value, $value, $field_id ){


		return $sanitized_value;
	}

	public function show_bulk_edit_button() {
		echo '<a href="#" id="modula-bulk-edit" class="button button-primary"><span class="dashicons dashicons-forms"></span>Bulk Edit</a>';
	}

	public function render_field_type( $html, $field, $value ) {

		$html = '<div class="content">';
		$html .= $field['content'];
		$html .= '</div>';


		return $html;
	}


	public function content_format( $format, $field ){

		$format = '<tr class="no-paddings" data-container="' . esc_attr( $field['id'] ) . '"><td colspan="2"><label class="th-label">%s</label>%s<div>%s</div></td></tr>';
		return $format;

	}

}

new Modula_PRO_Settings();