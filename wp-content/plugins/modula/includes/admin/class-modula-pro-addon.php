<?php

class Modula_PRO_Addon {

	private $slugs   = array(
		'modula-video'   => 'modula-video/modula-video.php',
		'modula-speedup' => 'modula-speedup/modula-speedup.php',
	);

	private $fetch_addons = false;
	private $site_host;
	private $download_urls = array();
	
	function __construct() {
		
		add_filter( 'modula_addon_button_action', array( $this, 'output_download_link' ), 10, 2 );
		add_filter( 'modula_addon_server_url', array( $this, 'add_license_to_url' ), 10, 2 );

		// Add script for installing addons
		add_action( 'admin_enqueue_scripts', array( $this, 'addons_scripts' ) );

		// Add ajax action in order to install our addons
		add_action( 'wp_ajax_modula-install-addons', array( $this, 'install_addons' ), 20 );

		// Get website domain
        if ( function_exists( 'domain_mapping_siteurl' ) ) {
            $this->site_host = domain_mapping_siteurl( get_current_blog_id() );
        } else {
            $this->site_host = site_url();
        }

        // Get License key
        $this->license_key = trim( get_option( 'modula_pro_license_key' ) );

	}

	private function check_for_addons() {

	    // // Make sure this matches the exact URL from your site.
	    $url = apply_filters( 'modula_addon_server_url', 'https://wp-modula.com/wp-json/mt/v1/get-addons' );
	    $url = add_query_arg( array(
	    	'license' => $this->license_key,
	    	'url'     => $this->site_host,
	    ), $url );

	    // Get data from the remote URL.
	    $response = wp_remote_get( $url );

	    if ( ! is_wp_error( $response ) ) {

	        // Decode the data that we got.
	        $data = json_decode( wp_remote_retrieve_body( $response ), true );

	        if ( ! empty( $data ) && is_array( $data ) ) {

	            // Store the data for a week.
	            $this->download_urls = $data;

	        }
	    }

	}

	public function addons_scripts( $hook ){

		if ( 'modula-gallery_page_modula-addons' == $hook ) {
			wp_enqueue_script( 'modula-pro-addon', MODULA_PRO_URL . 'assets/js/wp-modula-addons.js', array( 'jquery' ), '2.0.0', true );
			$args = array(
				'install_nonce' => wp_create_nonce( 'modula-pro-install' ),
				'connect_error' => esc_html__( 'ERROR: There was an error connecting to the server, Please try again.', 'modula' ),
			);
			wp_localize_script( 'modula-pro-addon', 'modulaPRO', $args );
		}

	}

	public function add_license_to_url( $url ) {
		return add_query_arg( array(
			'license' => get_option( 'modula_pro_license_key' ),
			'url'     => site_url(),
		), $url );
	}

	public function output_download_link( $link, $addon ) {

		if ( empty( $this->download_urls ) && '' != $this->license_key ) {
			$this->check_for_addons();
		}

		$labels = array(
			'install'   => esc_html__( 'Install & Activate', 'modula' ),
			'activate'  => esc_html__( 'Activate', 'modula' ),
			'installed' => esc_html__( 'Installed', 'modula' ),
		);

		$classes = array(
			'install'   => 'button button-primary modula-addon-action',
			'activate'  => 'button button-primary modula-addon-action',
			'installed' => 'button',
		);

		$action = 'install';

		if ( ! isset( $addon['slug'] ) ) {
			$url  = admin_url( 'edit.php?post_type=modula-gallery&page=modula&modula-tab=licenses' );
			$link = '<a href="' . $url . '" class="button button-primary">' . esc_html__( 'Add license', 'modula' ) . '</a>';
			return $link;
		}

		if ( isset( $this->download_urls[ $addon['slug'] ] ) ) {
			
			$slug = $addon['slug'];
			$plugin_path = $this->slugs[ $addon['slug'] ];

			if ( $this->check_plugin_is_installed( $addon['slug'] ) && ! $this->check_plugin_is_active( $plugin_path ) ) {
				$action = 'activate';
			}elseif ( $this->check_plugin_is_active( $plugin_path ) ) {
				$action = 'installed';
			}

			if ( 'install' != $action ) {
				$url = $this->create_plugin_link( $action, $plugin_path );
			}else{
				$url = $this->download_urls[ $addon['slug'] ];
			}

			$attr = '';

			if ( 'installed' != $action ) {
				$attr = 'data-action="' . $action . '"';
			}else{
				$attr = 'disabled="disabled"';
			}

			$link = '<a href="' . $url . '" ' . $attr . ' class="' . $classes[ $action ] . '">' . $labels[ $action ] . '</a>';

		}else{
			$url  = admin_url( 'edit.php?post_type=modula-gallery&page=modula&modula-tab=licenses' );
			$link = '<a href="' . $url . '" class="button button-primary">' . esc_html__( 'Add license', 'modula' ) . '</a>';
		}

		return $link;
	}

	// Function to check if is a plugin is active
	private function create_plugin_link( $state, $slug ) {
		$string = '';
		switch ( $state ) {
			case 'deactivate':
				$string = add_query_arg(
					array(
						'action'        => 'deactivate',
						'plugin'        => rawurlencode( $slug ),
						'plugin_status' => 'all',
						'paged'         => '1',
						'_wpnonce'      => wp_create_nonce( 'deactivate-plugin_' . $slug ),
					),
					admin_url( 'plugins.php' )
				);
				break;
			case 'activate':
				$string = add_query_arg(
					array(
						'action'        => 'activate',
						'plugin'        => rawurlencode( $slug ),
						'plugin_status' => 'all',
						'paged'         => '1',
						'_wpnonce'      => wp_create_nonce( 'activate-plugin_' . $slug ),
					),
					admin_url( 'plugins.php' )
				);
				break;
			default:
				$string = '';
				break;
		}// End switch().
		return $string;
	}

	private function _get_plugins( $plugin_folder = '' ) {

		if ( ! empty( $this->plugins ) ) {
			return $this->plugins;
		}

		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		$this->plugins = get_plugins( $plugin_folder );
		return $this->plugins;
	}

	private function check_plugin_is_installed( $slug ) {
		if ( file_exists( WP_PLUGIN_DIR . '/' . $slug ) ) {
			return true;
		}
		return false;
	}
	/**
	 * @return bool
	 */
	private function check_plugin_is_active( $plugin_path ) {
		if ( file_exists( WP_PLUGIN_DIR . '/' . $plugin_path ) ) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			return is_plugin_active( $plugin_path );
		}
	}

	// Install Addons
	public function install_addons() {

		// Run a security check first.
		check_admin_referer( 'modula-pro-install', 'nonce' );

		// Install the addon.
		if ( isset( $_POST['plugin'] ) ) {
			$download_url = $_POST['plugin'];
			global $hook_suffix;

			// Set the current screen to avoid undefined notices.
			set_current_screen();

			// Prepare variables.
			$method = '';
			$url    = add_query_arg(
				array(
					'page' => 'modula-pro-settings'
				),
				admin_url( 'admin.php' )
			);
			$url = esc_url( $url );

			// Start output bufferring to catch the filesystem form if credentials are needed.
			ob_start();
			if ( false === ( $creds = request_filesystem_credentials( $url, $method, false, false, null ) ) ) {
				$form = ob_get_clean();
				echo json_encode( array( 'form' => $form ) );
				die;
			}

			// If we are not authenticated, make it happen now.
			if ( ! WP_Filesystem( $creds ) ) {
				ob_start();
				request_filesystem_credentials( $url, $method, true, false, null );
				$form = ob_get_clean();
				echo json_encode( array( 'form' => $form ) );
				die;
			}

			// We do not need any extra credentials if we have gotten this far, so let's install the plugin.
			require_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
			require_once MODULA_PRO_PATH . 'includes/admin/class-modula-pro-skin.php';

			// Create the plugin upgrader with our custom skin.
			$installer = new Plugin_Upgrader( $skin = new Modula_PRO_Skin() );
			$installer->install( $download_url );

			// Flush the cache and return the newly installed plugin basename.
			wp_cache_flush();
			if ( $installer->plugin_info() ) {
				$plugin_basename = $installer->plugin_info();
				$activate_url = $this->create_plugin_link( 'activate' ,$plugin_basename );
				echo json_encode( array(
					'plugin'       => $plugin_basename,
					'activate_url' => $activate_url
				) );
				die;
			}
		}

		// Send back a response.
		echo json_encode( true );
		die;

	}

}

new Modula_PRO_Addon();