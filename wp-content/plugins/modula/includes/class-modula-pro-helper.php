<?php

/**
 * 
 */
class Modula_Pro_Helper {
	
	public static function remove_empty_items( $array ) {

		if ( ! is_array( $array ) ) {
			return false;
		}

		if ( empty( $array ) ) {
			return $array;
		}

		$new_array = array();

		foreach ( $array as $value ) {
			if ( '' != $value ) {
				$new_array[] = $value;
			}
		}

		return $new_array;

	}

	public static function get_plugins( $plugin_folder = '' ) {
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		return get_plugins( $plugin_folder );
	}

	public static function _get_plugin_basename_from_slug( $slug ) {
		$keys = array_keys( self::get_plugins() );

		foreach ( $keys as $key ) {
			if ( preg_match( '|^' . $slug . '/|', $key ) ) {
				return $key;
			}
		}

		return $slug;
	}
		
	public static function check_plugin_is_installed( $slug ) {
		$plugin_path = self::_get_plugin_basename_from_slug( $slug );
		if ( file_exists( WP_PLUGIN_DIR . '/' . $plugin_path ) ) {
			return true;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public static function check_plugin_is_active( $slug ) {
		$plugin_path = self::_get_plugin_basename_from_slug( $slug );
		if ( file_exists( WP_PLUGIN_DIR . '/' . $plugin_path ) ) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

			return is_plugin_active( $plugin_path );
		}
	}

}